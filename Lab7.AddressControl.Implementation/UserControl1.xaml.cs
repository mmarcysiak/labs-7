﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lab7.AddressControl.Contract;
using System.IO;
using System.Net;

namespace Lab7.AddressControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UserControl1 : UserControl ,IAddress
    {
        public UserControl1()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            String url = adressurl.Text;
            MemoryStream ms = new MemoryStream(new WebClient().DownloadData(new Uri(url)));
            ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            ImageSource imageSource = (ImageSource)imageSourceConverter.ConvertFrom(ms);
        }

        public new Control Control
        {
            get { throw new NotImplementedException(); }
        }

        public event EventHandler<AddressChangedArgs> AddressChanged;

    }
}
